﻿namespace MacRecord.Presentation
{
    partial class AboutMachineView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutMachineView));
            this.label1 = new System.Windows.Forms.Label();
            this.nazivTextBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tipComboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.brandComboBox2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.modelTextBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.godinaTextBox3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ukupnoTextBox4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.odServisaTextBox5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.limitTextBox6 = new System.Windows.Forms.TextBox();
            this.saveChangesButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.deleteButton = new System.Windows.Forms.Button();
            this.noviRadniSatiTextBox = new System.Windows.Forms.TextBox();
            this.noviSatiButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naziv stroja:";
            // 
            // nazivTextBox1
            // 
            this.nazivTextBox1.Location = new System.Drawing.Point(13, 25);
            this.nazivTextBox1.Name = "nazivTextBox1";
            this.nazivTextBox1.Size = new System.Drawing.Size(219, 20);
            this.nazivTextBox1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tip:";
            // 
            // tipComboBox1
            // 
            this.tipComboBox1.FormattingEnabled = true;
            this.tipComboBox1.Location = new System.Drawing.Point(13, 64);
            this.tipComboBox1.Name = "tipComboBox1";
            this.tipComboBox1.Size = new System.Drawing.Size(219, 21);
            this.tipComboBox1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Brand:";
            // 
            // brandComboBox2
            // 
            this.brandComboBox2.FormattingEnabled = true;
            this.brandComboBox2.Location = new System.Drawing.Point(13, 104);
            this.brandComboBox2.Name = "brandComboBox2";
            this.brandComboBox2.Size = new System.Drawing.Size(219, 21);
            this.brandComboBox2.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Model:";
            // 
            // modelTextBox2
            // 
            this.modelTextBox2.Location = new System.Drawing.Point(13, 144);
            this.modelTextBox2.Name = "modelTextBox2";
            this.modelTextBox2.Size = new System.Drawing.Size(219, 20);
            this.modelTextBox2.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Godina proizvodnje:";
            // 
            // godinaTextBox3
            // 
            this.godinaTextBox3.Location = new System.Drawing.Point(13, 183);
            this.godinaTextBox3.Name = "godinaTextBox3";
            this.godinaTextBox3.Size = new System.Drawing.Size(219, 20);
            this.godinaTextBox3.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Ukupno radnih sati:";
            // 
            // ukupnoTextBox4
            // 
            this.ukupnoTextBox4.Location = new System.Drawing.Point(13, 222);
            this.ukupnoTextBox4.Name = "ukupnoTextBox4";
            this.ukupnoTextBox4.Size = new System.Drawing.Size(219, 20);
            this.ukupnoTextBox4.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 245);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Sati od servisa";
            // 
            // odServisaTextBox5
            // 
            this.odServisaTextBox5.Location = new System.Drawing.Point(13, 261);
            this.odServisaTextBox5.Name = "odServisaTextBox5";
            this.odServisaTextBox5.Size = new System.Drawing.Size(219, 20);
            this.odServisaTextBox5.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 288);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Limit između 2 servisa:";
            // 
            // limitTextBox6
            // 
            this.limitTextBox6.Location = new System.Drawing.Point(13, 305);
            this.limitTextBox6.Name = "limitTextBox6";
            this.limitTextBox6.Size = new System.Drawing.Size(219, 20);
            this.limitTextBox6.TabIndex = 9;
            // 
            // saveChangesButton
            // 
            this.saveChangesButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.saveChangesButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.saveChangesButton.Location = new System.Drawing.Point(13, 335);
            this.saveChangesButton.Name = "saveChangesButton";
            this.saveChangesButton.Size = new System.Drawing.Size(113, 23);
            this.saveChangesButton.TabIndex = 10;
            this.saveChangesButton.Text = "Spremi Promjene";
            this.saveChangesButton.UseVisualStyleBackColor = false;
            this.saveChangesButton.Click += new System.EventHandler(this.saveChangesButton_Click);
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(11, 330);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(229, 2);
            this.label9.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(272, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 100);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Napomena";
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Location = new System.Drawing.Point(3, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(240, 81);
            this.label10.TabIndex = 0;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.deleteButton.Location = new System.Drawing.Point(157, 335);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 11;
            this.deleteButton.Text = "OBRIŠI";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // noviRadniSatiTextBox
            // 
            this.noviRadniSatiTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.noviRadniSatiTextBox.Location = new System.Drawing.Point(272, 120);
            this.noviRadniSatiTextBox.Multiline = true;
            this.noviRadniSatiTextBox.Name = "noviRadniSatiTextBox";
            this.noviRadniSatiTextBox.Size = new System.Drawing.Size(82, 44);
            this.noviRadniSatiTextBox.TabIndex = 0;
            // 
            // noviSatiButton
            // 
            this.noviSatiButton.Location = new System.Drawing.Point(360, 119);
            this.noviSatiButton.Name = "noviSatiButton";
            this.noviSatiButton.Size = new System.Drawing.Size(158, 45);
            this.noviSatiButton.TabIndex = 1;
            this.noviSatiButton.Text = "Novi Sati";
            this.noviSatiButton.UseVisualStyleBackColor = true;
            this.noviSatiButton.Click += new System.EventHandler(this.noviSatiButton_Click);
            // 
            // AboutMachineView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 368);
            this.Controls.Add(this.noviSatiButton);
            this.Controls.Add(this.noviRadniSatiTextBox);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.saveChangesButton);
            this.Controls.Add(this.limitTextBox6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.odServisaTextBox5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ukupnoTextBox4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.godinaTextBox3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.modelTextBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.brandComboBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tipComboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nazivTextBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AboutMachineView";
            this.Text = "Pojedinosti o stroju";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nazivTextBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox tipComboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox brandComboBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox modelTextBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox godinaTextBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ukupnoTextBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox odServisaTextBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox limitTextBox6;
        private System.Windows.Forms.Button saveChangesButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox noviRadniSatiTextBox;
        private System.Windows.Forms.Button noviSatiButton;
    }
}