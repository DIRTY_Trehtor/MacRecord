﻿namespace MacRecord.Presentation
{
    partial class ProcessServiceView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.strojComboBox = new System.Windows.Forms.ComboBox();
            this.datumTextBox = new System.Windows.Forms.TextBox();
            this.serviserTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.opisTextBox = new System.Windows.Forms.RichTextBox();
            this.dodajButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // strojComboBox
            // 
            this.strojComboBox.FormattingEnabled = true;
            this.strojComboBox.Location = new System.Drawing.Point(16, 29);
            this.strojComboBox.Name = "strojComboBox";
            this.strojComboBox.Size = new System.Drawing.Size(378, 21);
            this.strojComboBox.TabIndex = 0;
            // 
            // datumTextBox
            // 
            this.datumTextBox.Location = new System.Drawing.Point(16, 167);
            this.datumTextBox.Name = "datumTextBox";
            this.datumTextBox.Size = new System.Drawing.Size(378, 20);
            this.datumTextBox.TabIndex = 2;
            // 
            // serviserTextBox
            // 
            this.serviserTextBox.Location = new System.Drawing.Point(16, 206);
            this.serviserTextBox.Name = "serviserTextBox";
            this.serviserTextBox.Size = new System.Drawing.Size(378, 20);
            this.serviserTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Stroj koji se servisira:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Opis:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Datum:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Serviser:";
            // 
            // opisTextBox
            // 
            this.opisTextBox.Location = new System.Drawing.Point(16, 73);
            this.opisTextBox.Name = "opisTextBox";
            this.opisTextBox.Size = new System.Drawing.Size(378, 75);
            this.opisTextBox.TabIndex = 1;
            this.opisTextBox.Text = "";
            // 
            // dodajButton
            // 
            this.dodajButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.dodajButton.Location = new System.Drawing.Point(12, 337);
            this.dodajButton.Name = "dodajButton";
            this.dodajButton.Size = new System.Drawing.Size(382, 67);
            this.dodajButton.TabIndex = 4;
            this.dodajButton.Text = "Pohrani Servis";
            this.dodajButton.UseVisualStyleBackColor = true;
            this.dodajButton.Click += new System.EventHandler(this.dodajButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.groupBox1.Location = new System.Drawing.Point(16, 232);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 77);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Info";
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(3, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(372, 47);
            this.label5.TabIndex = 0;
            this.label5.Text = "Obradom servisa vezanog za neki stroj, radni sati od zadnjeg servisa se postavlja" +
    "ju na nulu.";
            // 
            // ProcessServiceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 416);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dodajButton);
            this.Controls.Add(this.opisTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.serviserTextBox);
            this.Controls.Add(this.datumTextBox);
            this.Controls.Add(this.strojComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ProcessServiceView";
            this.Text = "Odradi servis";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox strojComboBox;
        private System.Windows.Forms.TextBox datumTextBox;
        private System.Windows.Forms.TextBox serviserTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox opisTextBox;
        private System.Windows.Forms.Button dodajButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
    }
}