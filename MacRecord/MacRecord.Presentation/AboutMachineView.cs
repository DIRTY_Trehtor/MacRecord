﻿using MacRecord.BaseLib;
using MacRecord.Controllers;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Presentation
{
    public partial class AboutMachineView : Form, IAboutMachineView
    {
        private IMachinesRepository machinesRepository;
        private IAboutMachineController aboutMachineController;

        public AboutMachineView(IMachinesRepository machinesRepositor, IAboutMachineController aboutMachineController)
        {
            this.machinesRepository = machinesRepositor;
            this.aboutMachineController = aboutMachineController;

            InitializeComponent();
            setIntegerMaskToNeededFields();
            initializeComponentForSpecificMachine();

            //centriraj
            this.StartPosition = FormStartPosition.CenterParent;
        }
        private void setIntegerMaskToNeededFields()
        {
            this.noviRadniSatiTextBox.KeyPress += (s, e) => {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            };
        }
        private void initializeComponentForSpecificMachine()
        {
            Machine machine = this.aboutMachineController.getMachine();

            this.nazivTextBox1.Text = machine.name;
            setBrandComboBoxValues();
            setMachineTypeComboBoxValues();
            this.modelTextBox2.Text = machine.model;
            this.godinaTextBox3.Text = machine.year;
            this.ukupnoTextBox4.Text = machine.overallWorkingHours.ToString();
            this.odServisaTextBox5.Text = machine.recentHours.ToString();
            this.odServisaTextBox5.Enabled = false;
            this.limitTextBox6.Text = machine.betweenServiceLimit.ToString();
        }
        private void setBrandComboBoxValues()
        {
            Machine machine = this.aboutMachineController.getMachine();

            MachineBrand brandToSet = machine.brand;
            IList<MachineBrand> brands = this.machinesRepository.getMachineBrands();

            Dictionary<string, MachineBrand> brandsDict = new Dictionary<string, MachineBrand>();
            int i = 0;
            int index = 0;
            foreach (MachineBrand brand in brands)
            {
                brandsDict.Add(brand.name, brand);
                if (brand.Equals(machine.brand))
                    index = i;
                i++;
            }
            this.brandComboBox2.DataSource = new BindingSource(brandsDict, null);
            this.brandComboBox2.DisplayMember = "Key";
            this.brandComboBox2.ValueMember = "Value";

            brandComboBox2.SelectedIndex = index;

        }
        private void setMachineTypeComboBoxValues()
        {
            this.tipComboBox1.DataSource = new BindingSource(this.machinesRepository.getMachineTypes(), null);
            this.tipComboBox1.DisplayMember = "Value";
            this.tipComboBox1.ValueMember = "Key";

            int typeToSet = this.aboutMachineController.getMachine().type;

            tipComboBox1.SelectedValue = typeToSet;
        }
        //interface
        public DialogResult showDialog()
        {
            return this.ShowDialog();
        }
        /* INTERFACE */
        public string machineName => this.nazivTextBox1.Text;

        public int machineType
        {
            get
            {
                KeyValuePair<int, string> selectedEntry = (KeyValuePair<int, string>)tipComboBox1.SelectedItem;
                return selectedEntry.Key;
            }
        }

        public string machineModel => this.modelTextBox2.Text;

        public string manufYear => this.godinaTextBox3.Text;

        public string overallWorkingHours => this.ukupnoTextBox4.Text;

        public string recentHours => this.odServisaTextBox5.Text;

        public string betweenServiceLimit => this.limitTextBox6.Text;

        MachineBrand IAboutMachineView.machineBrand
        {
            get
            {
                if (this.brandComboBox2.SelectedValue == null)
                    return null;

                return (MachineBrand)this.brandComboBox2.SelectedValue;
            }
        }

        public string newWorkHours => this.noviRadniSatiTextBox.Text;

        private void saveChangesButton_Click(object sender, EventArgs e)
        {
            this.aboutMachineController.updateMachine();
            this.DialogResult = DialogResult.Ignore;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            this.aboutMachineController.deleteMachine();
            this.DialogResult = DialogResult.Abort;
        }

        private void noviSatiButton_Click(object sender, EventArgs e)
        {
            bool result = this.aboutMachineController.addWorkingHours();

            if (result)
            {
                this.odServisaTextBox5.Text = this.aboutMachineController.getMachine().recentHours.ToString();
                this.ukupnoTextBox4.Text = this.aboutMachineController.getMachine().overallWorkingHours.ToString();
                this.noviRadniSatiTextBox.Text = "";
            }
        }
    }
}
