﻿using MacRecord.BaseLib;
using MacRecord.Controllers;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.Presentation
{
    public class WindowFormsFactory : IWindowFormsFactory
    {
        public IHelpView createHelpView(IHelpController helpController)
        {
            HelpView newView = new HelpView(helpController);
            return newView;
        }
        public IAddMachineView createAddMachineView(IMachinesRepository machinesRepository)
        {
            AddMachineView newView = new AddMachineView(machinesRepository);
            return newView;
        }
        public IServicesView createServicesView(IMachinesRepository machinesRepository)
        {
            ServicesView newView = new ServicesView(machinesRepository);
            return newView;
        }
        public IProcessServiceView createProcessServiceView(IMachinesRepository machinesRepository, IProcessServiceController processServiceController)
        {
            ProcessServiceView newView = new ProcessServiceView(machinesRepository, processServiceController);
            return newView;
        }

        public IAboutMachineView createAboutMachineView(IMachinesRepository machinesRepository, IAboutMachineController aboutMachineController)
        {
            AboutMachineView newView = new AboutMachineView(machinesRepository, aboutMachineController);
            return newView;
        }

        public IPendingServicesView createPendingServicesView(IPendingServicesController pendingServicesController)
        {
            PendingServicesView newView = new PendingServicesView(pendingServicesController);
            return newView;
        }
    }
}
