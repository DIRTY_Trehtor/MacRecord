﻿namespace MacRecord.Presentation
{
    partial class AddMachineView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.limitTextBox = new System.Windows.Forms.TextBox();
            this.odServisaSatiTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.brandComboBox = new System.Windows.Forms.ComboBox();
            this.TipComboBox = new System.Windows.Forms.ComboBox();
            this.ukupnoSatiTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.godinaTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nazivStrojaTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.zemljaTexBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.nazivBrandaTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dodajStrojButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(505, 354);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.limitTextBox);
            this.panel1.Controls.Add(this.odServisaSatiTextBox);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.brandComboBox);
            this.panel1.Controls.Add(this.TipComboBox);
            this.panel1.Controls.Add(this.ukupnoSatiTextBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.godinaTextBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.modelTextBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.nazivStrojaTextBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(246, 348);
            this.panel1.TabIndex = 0;
            // 
            // limitTextBox
            // 
            this.limitTextBox.Location = new System.Drawing.Point(6, 292);
            this.limitTextBox.Name = "limitTextBox";
            this.limitTextBox.Size = new System.Drawing.Size(237, 20);
            this.limitTextBox.TabIndex = 7;
            // 
            // odServisaSatiTextBox
            // 
            this.odServisaSatiTextBox.Location = new System.Drawing.Point(6, 252);
            this.odServisaSatiTextBox.Name = "odServisaSatiTextBox";
            this.odServisaSatiTextBox.Size = new System.Drawing.Size(237, 20);
            this.odServisaSatiTextBox.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 275);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Limit sati između 2 servisa";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Sati od servisa:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Brand:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tip:";
            // 
            // brandComboBox
            // 
            this.brandComboBox.FormattingEnabled = true;
            this.brandComboBox.Location = new System.Drawing.Point(6, 95);
            this.brandComboBox.Name = "brandComboBox";
            this.brandComboBox.Size = new System.Drawing.Size(194, 21);
            this.brandComboBox.TabIndex = 2;
            // 
            // TipComboBox
            // 
            this.TipComboBox.FormattingEnabled = true;
            this.TipComboBox.Location = new System.Drawing.Point(6, 55);
            this.TipComboBox.Name = "TipComboBox";
            this.TipComboBox.Size = new System.Drawing.Size(194, 21);
            this.TipComboBox.TabIndex = 1;
            // 
            // ukupnoSatiTextBox
            // 
            this.ukupnoSatiTextBox.Location = new System.Drawing.Point(6, 213);
            this.ukupnoSatiTextBox.Name = "ukupnoSatiTextBox";
            this.ukupnoSatiTextBox.Size = new System.Drawing.Size(237, 20);
            this.ukupnoSatiTextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ukupno radnih sati:";
            // 
            // godinaTextBox
            // 
            this.godinaTextBox.Location = new System.Drawing.Point(6, 174);
            this.godinaTextBox.Name = "godinaTextBox";
            this.godinaTextBox.Size = new System.Drawing.Size(237, 20);
            this.godinaTextBox.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Godina proizvodnje:";
            // 
            // modelTextBox
            // 
            this.modelTextBox.Location = new System.Drawing.Point(6, 135);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.Size = new System.Drawing.Size(237, 20);
            this.modelTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Model:";
            // 
            // nazivStrojaTextBox
            // 
            this.nazivStrojaTextBox.Location = new System.Drawing.Point(6, 16);
            this.nazivStrojaTextBox.Name = "nazivStrojaTextBox";
            this.nazivStrojaTextBox.Size = new System.Drawing.Size(237, 20);
            this.nazivStrojaTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naziv stroja:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(255, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.08163F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70.91837F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(247, 348);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 95);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Napomena";
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(235, 76);
            this.label9.TabIndex = 0;
            this.label9.Text = "Ako do sad ne postoji niti jedan brand stroja, možete ga stvoriti na licu mjesta." +
    " Ako odaberete postojeći brand, a ujedno i popunite formu za dodavanje branda, d" +
    "odavanje novog branda ima prednost.";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.zemljaTexBox);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.nazivBrandaTextBox);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(241, 241);
            this.panel2.TabIndex = 1;
            // 
            // zemljaTexBox
            // 
            this.zemljaTexBox.Location = new System.Drawing.Point(7, 65);
            this.zemljaTexBox.Name = "zemljaTexBox";
            this.zemljaTexBox.Size = new System.Drawing.Size(231, 20);
            this.zemljaTexBox.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Zemlja podrijetla:";
            // 
            // nazivBrandaTextBox
            // 
            this.nazivBrandaTextBox.Location = new System.Drawing.Point(7, 21);
            this.nazivBrandaTextBox.Name = "nazivBrandaTextBox";
            this.nazivBrandaTextBox.Size = new System.Drawing.Size(231, 20);
            this.nazivBrandaTextBox.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Naziv branda:";
            // 
            // dodajStrojButton
            // 
            this.dodajStrojButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.dodajStrojButton.Location = new System.Drawing.Point(13, 373);
            this.dodajStrojButton.Name = "dodajStrojButton";
            this.dodajStrojButton.Size = new System.Drawing.Size(505, 67);
            this.dodajStrojButton.TabIndex = 1;
            this.dodajStrojButton.Text = "Dodaj Stroj";
            this.dodajStrojButton.UseVisualStyleBackColor = true;
            this.dodajStrojButton.Click += new System.EventHandler(this.dodajStrojButton_Click);
            // 
            // AddMachineView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 444);
            this.Controls.Add(this.dodajStrojButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddMachineView";
            this.Text = "Dodavanje stroja";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nazivStrojaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button dodajStrojButton;
        private System.Windows.Forms.TextBox ukupnoSatiTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox godinaTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox TipComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox brandComboBox;
        private System.Windows.Forms.TextBox limitTextBox;
        private System.Windows.Forms.TextBox odServisaSatiTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox zemljaTexBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox nazivBrandaTextBox;
        private System.Windows.Forms.Label label10;
    }
}