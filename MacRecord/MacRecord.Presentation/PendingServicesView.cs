﻿using MacRecord.BaseLib;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Presentation
{
    public partial class PendingServicesView : Form, IPendingServicesView
    {
        IPendingServicesController pendingServicesController;

        public PendingServicesView(IPendingServicesController pendingServicesController)
        {
            this.pendingServicesController = pendingServicesController;

            InitializeComponent();
            initializeListOfPendingServices();

            //centriraj
            this.StartPosition = FormStartPosition.CenterParent;
        }
        private void initializeListOfPendingServices()
        {
            this.zaServisFlowLayoutPanel.Controls.Clear();

            List<Machine> pendingServices = this.pendingServicesController.getPendingServices();
            int i = 0;
            foreach(Machine machine in pendingServices)
            {
                addOnePendingToList(machine, i);
                i++;
            }
        }
        private void addOnePendingToList(Machine machine, int index)
        {


            Panel newPanel = new Panel();
            newPanel.Name = "panel;" + index;
            newPanel.Size = new System.Drawing.Size(zaServisFlowLayoutPanel.Width - 50, 50);
            newPanel.TabIndex = index;
            newPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            newPanel.BackColor = Color.WhiteSmoke;

            Label label = new Label();
            label.Text = machine.name + "(" + machine.brand.name + "," + machine.model + ")" + " [" + machine.recentHours + "/" + machine.betweenServiceLimit + "]";
            label.Dock = DockStyle.Left;
            label.Width = 350;
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            label.AutoSize = false;
            label.TextAlign = ContentAlignment.MiddleLeft;

            Button newButtonProcess = new Button();
            newButtonProcess.Dock = DockStyle.Right;
            newButtonProcess.Name = "newButtonProcess;" + index;
            newButtonProcess.Text = "Procesiraj";
            newButtonProcess.UseVisualStyleBackColor = true;
            newButtonProcess.Padding = new Padding(0, 0, 5, 0);
            newButtonProcess.BackColor = System.Drawing.SystemColors.ActiveCaption;
            //dinamički obradi klik
            newButtonProcess.Click += (obj, eArgs) =>
            {
                this.pendingServicesController.processService(machine);
                initializeListOfPendingServices();
            };

            newPanel.Controls.Add(label);
            newPanel.Controls.Add(newButtonProcess);

            this.zaServisFlowLayoutPanel.Controls.Add(newPanel);

        }

        public bool showDialog()
        {
            if(this.ShowDialog() == DialogResult.OK)
            {
                return true;
            }
            return false;
        }
    }
}
