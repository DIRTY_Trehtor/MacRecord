﻿using MacRecord.BaseLib;
using MacRecord.Controllers;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Presentation
{
    public partial class ProcessServiceView : Form, IProcessServiceView
    {
        private IMachinesRepository machineRepository;
        IProcessServiceController processServiceController;

        public ProcessServiceView(IMachinesRepository machineRepository, IProcessServiceController processServiceController)
        {
            this.machineRepository = machineRepository;
            this.processServiceController = processServiceController;

            InitializeComponent();
            loadMachinesToComboBox();

            Machine machine = this.processServiceController.getMachine();
            if (machine != null)
                initializeForSpecificMachine(machine);

            //centriraj
            this.StartPosition = FormStartPosition.CenterParent;
        }
        private void loadMachinesToComboBox()
        {
            IList<Machine> machines = this.machineRepository.getMachines();

            if(machines.Count == 0)
            {
                this.strojComboBox.Enabled = false;
                return;
            }

            Dictionary<string, Machine> machineComboDict = new Dictionary<string, Machine>();
            foreach (Machine machine in machines)
            {
                machineComboDict.Add(machine.name + ", " + machine.brand.name + ", " + machine.model, machine);
            }
            strojComboBox.DataSource = new BindingSource(machineComboDict, null);
            strojComboBox.DisplayMember = "Key";
            strojComboBox.ValueMember = "Value";
            
        }
        private void initializeForSpecificMachine(Machine machine)
        {
            this.strojComboBox.SelectedValue = machine;
            this.strojComboBox.Enabled = false;
        }


        /* INTERFACE */
        public string opis => this.opisTextBox.Text;

        public string datum => this.datumTextBox.Text;

        public string serviser => this.serviserTextBox.Text;

        public Machine machine => (Machine)this.strojComboBox.SelectedValue;

        public bool showDialog()
        {
            if (this.ShowDialog() == DialogResult.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void dodajButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
