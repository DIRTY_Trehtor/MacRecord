﻿using MacRecord.BaseLib;
using MacRecord.Controllers;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Presentation
{
    public partial class AddMachineView : Form, IAddMachineView
    {
        private AddMachineController addMachineController;
        private IMachinesRepository machinesRepository;

        public AddMachineView(IMachinesRepository machinesRepository)
        {
            IWindowFormsFactory windowFormsFactory = new WindowFormsFactory();
            this.machinesRepository = machinesRepository;
            this.addMachineController = new AddMachineController(windowFormsFactory, machinesRepository);

            InitializeComponent();
            
            setIntegerMaskToNeededFields();

            initializeComboBoxes(machinesRepository);

            //centriraj
            this.StartPosition = FormStartPosition.CenterParent;
        }
        private void setIntegerMaskToNeededFields()
        {
            this.limitTextBox.KeyPress += (s, e) => {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            };
            this.odServisaSatiTextBox.KeyPress += (s, e) => {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            };
            this.ukupnoSatiTextBox.KeyPress += (s, e) => {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            };
        }
        private void initializeComboBoxes(IMachinesRepository machinesRepository)
        {
            
            IList<MachineBrand> brands = machinesRepository.getMachineBrands();

            if (brands.Count == 0)
            {
                brandComboBox.Enabled = false;
            }
            else
            {
                brandComboBox.Enabled = true;

                Dictionary<string, MachineBrand> brandComboDict = new Dictionary<string, MachineBrand>();
                foreach (MachineBrand brand in brands)
                {
                    string brandName = brand.name;
                    brandComboDict.Add(brandName, brand);
                }

                brandComboBox.DataSource = new BindingSource(brandComboDict, null);
                brandComboBox.DisplayMember = "Key";
                brandComboBox.ValueMember = "Value";
            }
            

            
            TipComboBox.DataSource = new BindingSource(this.machinesRepository.getMachineTypes(), null);
            TipComboBox.DisplayMember = "Value";
            TipComboBox.ValueMember = "Key";

        }

        /* INTERFACE */
        public string machineName => this.nazivStrojaTextBox.Text;

        public int machineType {
            get
            {
                if (this.TipComboBox.SelectedValue == null)
                    return 0;

                return (int)this.TipComboBox.SelectedValue;
            }
        } 

        public string machineModel => this.modelTextBox.Text;

        public string manufYear => this.godinaTextBox.Text;

        public string overallWorkingHours => this.ukupnoSatiTextBox.Text;

        public string recentHours => this.odServisaSatiTextBox.Text;

        public string betweenServiceLimit => this.limitTextBox.Text;

        MachineBrand IAddMachineView.machineBrand{
            get {
                if (this.brandComboBox.SelectedValue == null)
                    return null;

                return  (MachineBrand)this.brandComboBox.SelectedValue;
            }
        }

        string IAddMachineView.brandName => this.nazivBrandaTextBox.Text;

        string IAddMachineView.brandCoutry => this.zemljaTexBox.Text;

        public bool showDialog()
        {
            if (this.ShowDialog() == DialogResult.OK)
                return true;
            return false;
        }

        /* AKCIJE */
        private void dodajStrojButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
