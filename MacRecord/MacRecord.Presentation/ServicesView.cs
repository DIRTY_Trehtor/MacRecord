﻿using MacRecord.BaseLib;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Presentation
{
    public partial class ServicesView : Form, IServicesView
    {
        private IMachinesRepository machinesRepository;

        public ServicesView(IMachinesRepository machinesRepository)
        {
            this.machinesRepository = machinesRepository;

            InitializeComponent();
            initializeServicesList();

            //centriraj
            this.StartPosition = FormStartPosition.CenterParent;
        }

        public bool showDialog()
        {
            if (this.ShowDialog() == DialogResult.OK)
                return true;
            else
                return false;
        }

        private void initializeServicesList()
        {
            //očisti panel
            this.ServisiFlowLayoutPanel.Controls.Clear();

            int i = 0;
            IList<Service> services = this.machinesRepository.getServices();
            foreach (Service service in services)
            {
                insertOneServicePanel(service, i);
                i++;
            }
        }
        private void insertOneServicePanel(Service service, int index)
        {
            Panel newPanel = new Panel();
            newPanel.Name = "panel;" + service.id;
            newPanel.Size = new System.Drawing.Size(ServisiFlowLayoutPanel.Width - 50, 100);
            newPanel.TabIndex = index;
            newPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            newPanel.BackColor = Color.WhiteSmoke;

            Machine machine = service.servicedMachine;
            String text = "";
            text += machine.name + ", " + machine.brand.name + "(brand)," + machine.model + "(model)\r\n";
            text += "OPIS: " + service.workMade + "\r\nSERVISER: " + service.serviceGuyInformation;

            Label label = new Label();
            label.Text = text; //deni tekst
            label.Dock = DockStyle.Left;
            label.Width = ServisiFlowLayoutPanel.Width - 60;
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            label.AutoSize = false;
            label.TextAlign = ContentAlignment.MiddleLeft;

            newPanel.Controls.Add(label);
            this.ServisiFlowLayoutPanel.Controls.Add(newPanel);
        }
    }
}
