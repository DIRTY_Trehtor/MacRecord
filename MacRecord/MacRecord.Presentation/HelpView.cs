﻿using MacRecord.BaseLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Presentation
{
    public partial class HelpView : Form, IHelpView
    {
        private IHelpController helpController;

        public HelpView(IHelpController helpController)
        {
            this.helpController = helpController;

            InitializeComponent();

            this.aboutLabel.Text = helpController.getAboutInfo();
            this.manualLabel.Text = helpController.getManualInfo();

            //centriraj
            this.StartPosition = FormStartPosition.CenterParent;
        }

        public bool showDialog()
        {
            if (this.ShowDialog() == DialogResult.OK)
                return true;
            return false;
        }
    }
}
