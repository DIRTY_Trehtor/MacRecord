﻿using MacRecord.BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.Controllers
{
    class ServicesController
    {
        private IMachinesRepository machinesRepository;

        public ServicesController(IMachinesRepository machinesRepository)
        {
            this.machinesRepository = machinesRepository;
        }

        public void ShowServices(IServicesView newView)
        {
            newView.showDialog();
        }
    }
}
