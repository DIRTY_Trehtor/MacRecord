﻿using MacRecord.BaseLib;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Controllers
{
    public class AddMachineController
    {

        private IWindowFormsFactory windowFormsFactory;
        private IMachinesRepository machinesRepository;

        public AddMachineController(IWindowFormsFactory windowFormsFactory, IMachinesRepository machinesRepository)
        {
            this.windowFormsFactory = windowFormsFactory;
            this.machinesRepository = machinesRepository;
        }
        public void AddMachine(IAddMachineView inView)
        {
            if (inView.showDialog())
            {
                string name = inView.machineName;
                string model = inView.machineModel;
                MachineBrand brand = inView.machineBrand;
                int machineType = inView.machineType;
                string year = inView.manufYear;
                string overallWorkingHours = inView.overallWorkingHours;
                string recentHours = inView.recentHours;
                string betweenServiceLimit = inView.betweenServiceLimit;

                string brandName = inView.brandName;
                string brandCountry = inView.brandCoutry;

                if("".Equals(name) || "".Equals(model) || ("".Equals(brandName) || "".Equals(brandCountry)) && (brand == null) || "".Equals(year) ||
                    "".Equals(overallWorkingHours) || "".Equals(recentHours) || "".Equals(betweenServiceLimit)){

                    MessageBox.Show("Sve mora biti popunjeno.");
                    return;
                }

                MachineBrand machineBrand;
                if (brand == null || (!brandName.Equals("") && !brandCountry.Equals("")))
                {
                    MachineBrand newBrand = new MachineBrand(brandName, brandCountry);
                    this.machinesRepository.addMachineBrand(newBrand);
                    machineBrand = newBrand;
                    //addMachineBrandToDataBase(brand);
                }
                else
                {
                    machineBrand = brand;
                }
                
                Machine newMachine = new Machine(name, model, machineType, machineBrand, year, Int32.Parse(overallWorkingHours), Int32.Parse(recentHours), Int32.Parse(betweenServiceLimit));

                //adding db + repostitory
                this.machinesRepository.addMachine(newMachine);
                //checking for pending
                this.machinesRepository.tryMachineAsPending(newMachine);
            }
        }

        private MachineBrand findBrandByName(string name)
        {
            string[] splitted = name.Split(';');
            string brandName = splitted[0];
            string brandCountry = splitted[1];

            IList<MachineBrand> brands = this.machinesRepository.getMachineBrands();
            foreach (MachineBrand brand in brands)
            {
                if (brand.name.Equals(brandName) && brand.country.Equals(brandCountry))
                    return brand;
            }

            return null;
        }
        private void addMachineToDataBase(Machine machine)
        {
            throw new NotImplementedException();
        }
        private void addMachineBrandToDataBase(MachineBrand machineBrand)
        {
            throw new NotImplementedException();
        }
    }
}
