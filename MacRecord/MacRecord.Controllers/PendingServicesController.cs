﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MacRecord.BaseLib;
using MacRecord.Model;

namespace MacRecord.Controllers
{
    class PendingServicesController : IPendingServicesController
    {
        IMachinesRepository machinesRepository;
        IWindowFormsFactory windowFormsFactory;

        public PendingServicesController(IWindowFormsFactory windowFormsFactory, IMachinesRepository machinesRepository)
        {
            this.windowFormsFactory = windowFormsFactory;
            this.machinesRepository = machinesRepository;
        }

        /* INTERFACE */
        public void pendingServices(IPendingServicesView inView)
        {
            inView.showDialog();
        }
        public List<Machine> getPendingServices()
        {
            return this.machinesRepository.getPendingServices();
        }
        public void processService(Machine machine)
        {
            IProcessServiceController newController = new ProcessServiceController(this.machinesRepository, machine); 
            IProcessServiceView newView = windowFormsFactory.createProcessServiceView(this.machinesRepository, newController);

            newController.ProcessService(newView);
        }

    }
}
