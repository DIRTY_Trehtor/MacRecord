﻿using MacRecord.BaseLib;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Controllers
{
    public class AboutMachineController : IAboutMachineController
    {
        private IWindowFormsFactory windowFormsFactory;
        private IMachinesRepository machineRepository;
        private IAboutMachineView inView;
        private Machine machine;

        public AboutMachineController(IWindowFormsFactory windowFormsFactory, IMachinesRepository machineRepository, Machine machine)
        {
            this.windowFormsFactory = windowFormsFactory;
            this.machineRepository = machineRepository;
            this.machine = machine;
        }

        public void AboutMachine(IAboutMachineView inView)
        {
            this.inView = inView;
            switch (inView.showDialog())
            {
                case DialogResult.Ignore:
                    this.AboutMachine(inView);
                    break;
                default:

                    break;
            }

        }
        public Machine getMachine()
        {
            return this.machine;
        }
        public void updateMachine()
        {
            string name = inView.machineName;
            string model = inView.machineModel;
            MachineBrand brand = inView.machineBrand;
            int type = inView.machineType;
            string year = inView.manufYear;
            int overallWorkingHours = Int32.Parse(inView.overallWorkingHours);
            int recentHours = Int32.Parse(inView.recentHours);
            int betweenServiceLimit = Int32.Parse(inView.betweenServiceLimit);
            Machine newData = new Machine(name, model, type, brand, year, overallWorkingHours, recentHours, betweenServiceLimit);
            newData.id = this.machine.id;

            if(this.machine.Equals(newData))
            {
                MessageBox.Show("NEMA promjene.");
            }
            else
            {   
                //zapamti index starog zapisa
                int index = this.machineRepository.getMachines().IndexOf(this.machine);
                //spremi lokalno novog
                this.machine = newData;
                //spremi globalno novog
                this.machineRepository.updateMachine(newData, index);

                MessageBox.Show("Promjena SPREMLJENA.");
            }


        }
        public void deleteMachine()
        {
            this.machineRepository.deleteMachine(this.machine);
            this.machine = null;

            MessageBox.Show("Zapis je OBRISAN.");
        }
        

        public bool addWorkingHours()
        {
            string text = inView.newWorkHours;
            if ("".Equals(text))
                return false;

            int newHours = Int32.Parse(text);
            addNewWorkingHoursToMachine(newHours);

            return true;
        }
        private void addNewWorkingHoursToMachine(int newHours)
        {
            //find old machine
            int index = this.machineRepository.getMachines().IndexOf(this.machine);
            //change hours
            this.machine.recentHours += newHours;
            this.machine.overallWorkingHours += newHours;

            this.machineRepository.tryMachineAsPending(this.machine);

            //repo + DB
            this.machineRepository.updateMachine(this.machine, index);
        }
    }
}
