﻿using MacRecord.BaseLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.Controllers
{
    class HelpController : IHelpController
    {
        string about = 
            @"Aplikacija MacRecord je aplikacija osmišljena s ciljem da korisniku olakša vođenje evidencije o radu i sevisiranju strojeva, alata ili bilo kakvih drugih pomagala kojima su sati radi glavna jedinica mjere potrošenosti. Korisniku nudi dodavanje novih strojeva te njihovih proizvođača strojeva. Nudi mogućnost izmjene postojećih zapisa, zapisivanja novih sati rada nekog stroja, te dojave o nadolazećim servisima.";
        string manual = 
            @"Ukoliko ste prvi puta pokrenuli aplikaciju, predlažemo Vam da u glavnom prozoru pritisnete gumb 'Dodaj Stroj' kojim će se otvoriti forma za dodavanje stroja. Dodavanjem novog stroja, daje se mogućnost praćenja rada toga stroja. Korisnik tokom vremena unaša postepene sate odrađene na tom stroju. Navigirajući se glavnim izbornikom u glavnom pogledu, korisnik može odabrati karticu 'Servisi' gdje u podcjelinama dobiva uvid u predstojeće servise, ili zabilježiti nenadani servis.
Pravljenje zapisa o novim odrađenim radnim satima nekog stroja vrši se u glavnoj formi u popisu svih strojeva, ili u formi koja pokazuje detalje nekog stroja.";

        public HelpController() { }

        public void Help(IHelpView inView)
        {
            inView.showDialog();
        }

        public string getAboutInfo()
        {
            return this.about;
        }

        public string getManualInfo()
        {
            return this.manual;
        }
    }
}
