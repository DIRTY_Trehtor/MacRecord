﻿using MacRecord.BaseLib;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.Controllers
{
    public class ProcessServiceController : IProcessServiceController
    {
        private IMachinesRepository machineRepository;
        private Machine machine;

        public ProcessServiceController(IMachinesRepository machineRepository, Machine machine)
        {
            this.machineRepository = machineRepository;
            this.machine = machine;
        }

        public void ProcessService(IProcessServiceView inView)
        {
            if (inView.showDialog())
            {
                string opis = inView.opis;
                string datum = inView.datum;
                string serviser = inView.serviser;

                machine = inView.machine;


                if(machine == null || "".Equals(opis) || "".Equals(datum) || "".Equals(serviser))
                {
                    MessageBox.Show("Sve mora biti popunjeno.");
                    return;
                }

                Service service = new Service(this.machine, opis, datum, serviser);
                this.machineRepository.addService(service);
            }
        }

        public Machine getMachine()
        {
            return this.machine;
        }
    }
}
