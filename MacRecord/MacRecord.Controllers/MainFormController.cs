﻿using MacRecord.BaseLib;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.Controllers
{
    public class MainFormController
    {
        private IWindowFormsFactory windowFormsFactory;
        private IMachinesRepository machineRepository;

        private List<Machine> machinesToShow;

        public MainFormController(IWindowFormsFactory windowFormsFactory, IMachinesRepository machineRepository)
        {
            this.windowFormsFactory = windowFormsFactory;
            this.machineRepository = machineRepository;

            setInitialMachinesToShow();
        }

        private void setInitialMachinesToShow()
        {
            this.machinesToShow = new List<Machine>();

            IList<Machine> machines = this.machineRepository.getMachines();
            foreach (Machine m in machines)
            {
                this.machinesToShow.Add(m);
            }
        }

        /* OTVARANJE NOVIH FORMI */
        public void openHelpView()
        {
            HelpController newController = new HelpController();
            IHelpView newView = this.windowFormsFactory.createHelpView(newController);

            newController.Help(newView);
        }
        public void openPendingServicesView()
        {
            PendingServicesController newController = new PendingServicesController(this.windowFormsFactory, this.machineRepository);
            IPendingServicesView newView = this.windowFormsFactory.createPendingServicesView(newController);

            newController.pendingServices(newView);
        }
        public void openAboutMachineView(Machine machine)
        {
            AboutMachineController newController = new AboutMachineController(this.windowFormsFactory, this.machineRepository, machine);
            IAboutMachineView newView = this.windowFormsFactory.createAboutMachineView(this.machineRepository, newController);

            newController.AboutMachine(newView);

            setInitialMachinesToShow();
        }
        public void openAddMachineView()
        {
            AddMachineController newController = new AddMachineController(this.windowFormsFactory, this.machineRepository);
            IAddMachineView newView = this.windowFormsFactory.createAddMachineView(this.machineRepository);

            newController.AddMachine(newView);

            setInitialMachinesToShow();
        }   
        public void openServicesView()
        {
            ServicesController newController = new ServicesController(this.machineRepository);
            IServicesView newView = this.windowFormsFactory.createServicesView(this.machineRepository);

            newController.ShowServices(newView);
        }
        public void openProcessServiceView()
        {
            ProcessServiceController newController = new ProcessServiceController(this.machineRepository, null);
            IProcessServiceView newView = this.windowFormsFactory.createProcessServiceView(this.machineRepository, newController);

            newController.ProcessService(newView);
        }

        /**/
        public Dictionary<int, string> getMachineTypes()
        {
            return this.machineRepository.getMachineTypes();
        }
        public IList<MachineBrand> getMachineBrands()
        {
            return this.machineRepository.getMachineBrands();
        }
        public List<Machine> getMachines()
        {
            return this.machinesToShow;
        }
        public IList<Service> getServices()
        {
            return this.machineRepository.getServices();
        }
        public List<Machine> getPendingServices()
        {
            return this.machineRepository.getPendingServices();
        }
        public void deleteMachine(Machine machine)
        {
            this.machinesToShow.Remove(machine);

            AboutMachineController aboutMachineController = new AboutMachineController(this.windowFormsFactory, machineRepository, machine);
            aboutMachineController.deleteMachine();
        }
        public bool addNewWorkingHours(Machine machine, string value)
        {
            if ("".Equals(value))//empty field
            {
                return false;
            }

            int index = this.machineRepository.getMachines().IndexOf(machine);
            int newWorkingHours = Int32.Parse(value);
            machine.recentHours += newWorkingHours;
            machine.overallWorkingHours += newWorkingHours;

            //it only adds if it fits requirements
            this.machineRepository.tryMachineAsPending(machine);
            //update db + repository
            this.machineRepository.updateMachine(machine, index);

            return true;
        }
        public void setFilter(int tip, MachineBrand brand, int ukupnoVise, int ukupnoManje)
        {
            this.machinesToShow = new List<Machine>();

            if (ukupnoManje == -1)
                ukupnoManje = Int32.MaxValue;

            IList<Machine> machines = this.machineRepository.getMachines();

            foreach (Machine machine in machines)
            {
                if(brand != null)
                {
                    if(tip == -1)
                    {
                        if (machine.brand.Equals(brand))
                            if (machine.overallWorkingHours < ukupnoManje && machine.overallWorkingHours > ukupnoVise)
                                this.machinesToShow.Add(machine);
                    }
                    else
                    {
                        if (machine.type == tip && machine.brand.Equals(brand))
                            if (machine.overallWorkingHours < ukupnoManje && machine.overallWorkingHours > ukupnoVise)
                                this.machinesToShow.Add(machine);
                    }
                    
                }
                else
                {
                    
                    if (tip == -1)
                    {
                        if (machine.overallWorkingHours < ukupnoManje && machine.overallWorkingHours > ukupnoVise)
                            this.machinesToShow.Add(machine);
                    }
                    else
                    {
                        if (machine.type == tip)
                            if (machine.overallWorkingHours < ukupnoManje && machine.overallWorkingHours > ukupnoVise)
                                this.machinesToShow.Add(machine);
                    }
                }
                    
                
            }
        }
        public void removeFilter()
        {
            setInitialMachinesToShow();
        }
    }
}
