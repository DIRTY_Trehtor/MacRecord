﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.Model
{
    public class Machine
    {
        public virtual int id { get; set; }

        public virtual string name { get; set; }
        public virtual string model { get; set; }
        public virtual int type { get; set; }
        public virtual MachineBrand brand { get; set; }
        public virtual string year { get; set; }
        public virtual int overallWorkingHours { get; set; }
        public virtual int recentHours { get; set; }
        public virtual int betweenServiceLimit { get; set; }

        public Machine() { }

        public Machine(string name, string model, int type, MachineBrand brand, string year, int overallWorkingHours, int recentHours, int betweenServiceLimit)
        {
            this.name = name;
            this.model = model;
            this.type = type;
            this.brand = brand;
            this.year = year;
            this.overallWorkingHours = overallWorkingHours;
            this.recentHours = recentHours;
            this.betweenServiceLimit = betweenServiceLimit;

        }

        public override bool Equals(object obj)
        {
            var item = obj as Machine;

            if (item == null)
            {
                return false;
            }

            bool result = this.name.Equals(item.name) && this.model.Equals(item.model) && this.brand.Equals(item.brand) && this.type == item.type && this.year.Equals(item.year)
                && this.overallWorkingHours == item.overallWorkingHours && this.recentHours == item.recentHours && this.betweenServiceLimit == item.betweenServiceLimit;

            return result;
        }
    }
}
