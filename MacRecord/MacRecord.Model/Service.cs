﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.Model
{
    public class Service
    {
        public virtual int id { get; set; }

        public virtual Machine servicedMachine{ get; set; }
        public virtual string workMade{ get; set; }
        public virtual string date { get; set; }
        public virtual string serviceGuyInformation { get; set; }

        public Service() { }

        public Service(Machine servicedMachine, string workMade, string date, string serviceGuyInformation)
        {
            this.servicedMachine = servicedMachine;
            this.workMade = workMade;
            this.date = date;
            this.serviceGuyInformation = serviceGuyInformation;
        }
    }
}
