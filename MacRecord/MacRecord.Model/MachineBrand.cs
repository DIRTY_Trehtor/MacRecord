﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.Model
{
    public class MachineBrand
    {
        public virtual int id { get; set; }

        public virtual string name { get; set; }
        public virtual string country { get; set; }

        public MachineBrand() { }

        public MachineBrand(string name, string country)
        {
            this.name = name;
            this.country = country;
        }

        public override bool Equals(object obj)
        {
            var item = obj as MachineBrand;

            if (item == null)
            {
                return false;
            }

            bool result = this.name.Equals(item.name) && this.country.Equals(item.country);

            return result;
        }
    }
}
