﻿namespace MacRecord.AppStarter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.servisiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nadolazeciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nenadaniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obavljeniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistikaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomoćToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.dodajStrojButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutStrojevi = new System.Windows.Forms.FlowLayoutPanel();
            this.filterPanel = new System.Windows.Forms.Panel();
            this.manjeOdTextBox = new System.Windows.Forms.TextBox();
            this.viseOdTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ukloniFilterButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.postaviFilterButton = new System.Windows.Forms.Button();
            this.tipComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.brandComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mainMenu.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.filterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // mainMenu
            // 
            this.mainMenu.BackColor = System.Drawing.SystemColors.Control;
            this.mainMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.mainMenu.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.mainMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.servisiToolStripMenuItem,
            this.statistikaToolStripMenuItem,
            this.pomoćToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(9, 9);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(279, 36);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "menuStrip1";
            // 
            // servisiToolStripMenuItem
            // 
            this.servisiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nadolazeciToolStripMenuItem,
            this.nenadaniToolStripMenuItem,
            this.obavljeniToolStripMenuItem});
            this.servisiToolStripMenuItem.Name = "servisiToolStripMenuItem";
            this.servisiToolStripMenuItem.Size = new System.Drawing.Size(80, 32);
            this.servisiToolStripMenuItem.Text = "Servisi";
            // 
            // nadolazeciToolStripMenuItem
            // 
            this.nadolazeciToolStripMenuItem.Name = "nadolazeciToolStripMenuItem";
            this.nadolazeciToolStripMenuItem.Size = new System.Drawing.Size(181, 32);
            this.nadolazeciToolStripMenuItem.Text = "Nadolazeći";
            this.nadolazeciToolStripMenuItem.Click += new System.EventHandler(this.nadolazeciToolStripMenuItem_Click);
            // 
            // nenadaniToolStripMenuItem
            // 
            this.nenadaniToolStripMenuItem.Name = "nenadaniToolStripMenuItem";
            this.nenadaniToolStripMenuItem.Size = new System.Drawing.Size(181, 32);
            this.nenadaniToolStripMenuItem.Text = "Nenadani";
            this.nenadaniToolStripMenuItem.Click += new System.EventHandler(this.nenadaniToolStripMenuItem_Click);
            // 
            // obavljeniToolStripMenuItem
            // 
            this.obavljeniToolStripMenuItem.Name = "obavljeniToolStripMenuItem";
            this.obavljeniToolStripMenuItem.Size = new System.Drawing.Size(181, 32);
            this.obavljeniToolStripMenuItem.Text = "Obavljeni";
            this.obavljeniToolStripMenuItem.Click += new System.EventHandler(this.obavljeniToolStripMenuItem_Click);
            // 
            // statistikaToolStripMenuItem
            // 
            this.statistikaToolStripMenuItem.Name = "statistikaToolStripMenuItem";
            this.statistikaToolStripMenuItem.Size = new System.Drawing.Size(103, 32);
            this.statistikaToolStripMenuItem.Text = "Statistika";
            this.statistikaToolStripMenuItem.Click += new System.EventHandler(this.statistikaToolStripMenuItem_Click);
            // 
            // pomoćToolStripMenuItem
            // 
            this.pomoćToolStripMenuItem.Name = "pomoćToolStripMenuItem";
            this.pomoćToolStripMenuItem.Size = new System.Drawing.Size(84, 32);
            this.pomoćToolStripMenuItem.Text = "Pomoć";
            this.pomoćToolStripMenuItem.Click += new System.EventHandler(this.pomoćToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1040, 2);
            this.label1.TabIndex = 3;
            // 
            // dodajStrojButton
            // 
            this.dodajStrojButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.dodajStrojButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.dodajStrojButton.Location = new System.Drawing.Point(902, 12);
            this.dodajStrojButton.Name = "dodajStrojButton";
            this.dodajStrojButton.Size = new System.Drawing.Size(150, 38);
            this.dodajStrojButton.TabIndex = 4;
            this.dodajStrojButton.Text = "Dodaj Stroj";
            this.dodajStrojButton.UseVisualStyleBackColor = false;
            this.dodajStrojButton.Click += new System.EventHandler(this.dodajStrojButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.13461F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.86539F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutStrojevi, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.filterPanel, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 58);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1040, 436);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // flowLayoutStrojevi
            // 
            this.flowLayoutStrojevi.AutoScroll = true;
            this.flowLayoutStrojevi.BackColor = System.Drawing.SystemColors.ControlLight;
            this.flowLayoutStrojevi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutStrojevi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutStrojevi.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutStrojevi.Name = "flowLayoutStrojevi";
            this.flowLayoutStrojevi.Size = new System.Drawing.Size(764, 430);
            this.flowLayoutStrojevi.TabIndex = 0;
            // 
            // filterPanel
            // 
            this.filterPanel.Controls.Add(this.manjeOdTextBox);
            this.filterPanel.Controls.Add(this.viseOdTextBox);
            this.filterPanel.Controls.Add(this.label7);
            this.filterPanel.Controls.Add(this.label6);
            this.filterPanel.Controls.Add(this.ukloniFilterButton);
            this.filterPanel.Controls.Add(this.label5);
            this.filterPanel.Controls.Add(this.postaviFilterButton);
            this.filterPanel.Controls.Add(this.tipComboBox);
            this.filterPanel.Controls.Add(this.label4);
            this.filterPanel.Controls.Add(this.brandComboBox);
            this.filterPanel.Controls.Add(this.label3);
            this.filterPanel.Controls.Add(this.label2);
            this.filterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterPanel.Location = new System.Drawing.Point(773, 3);
            this.filterPanel.Name = "filterPanel";
            this.filterPanel.Size = new System.Drawing.Size(264, 430);
            this.filterPanel.TabIndex = 1;
            // 
            // manjeOdTextBox
            // 
            this.manjeOdTextBox.Location = new System.Drawing.Point(129, 127);
            this.manjeOdTextBox.Name = "manjeOdTextBox";
            this.manjeOdTextBox.Size = new System.Drawing.Size(113, 20);
            this.manjeOdTextBox.TabIndex = 11;
            // 
            // viseOdTextBox
            // 
            this.viseOdTextBox.Location = new System.Drawing.Point(6, 128);
            this.viseOdTextBox.Name = "viseOdTextBox";
            this.viseOdTextBox.Size = new System.Drawing.Size(113, 20);
            this.viseOdTextBox.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(126, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Ukupno sati MANJE OD:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Location = new System.Drawing.Point(3, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Ukupno sati VIŠE OD:";
            // 
            // ukloniFilterButton
            // 
            this.ukloniFilterButton.Location = new System.Drawing.Point(4, 404);
            this.ukloniFilterButton.Name = "ukloniFilterButton";
            this.ukloniFilterButton.Size = new System.Drawing.Size(257, 23);
            this.ukloniFilterButton.TabIndex = 7;
            this.ukloniFilterButton.Text = "Obriši SVE Filtre";
            this.ukloniFilterButton.UseVisualStyleBackColor = true;
            this.ukloniFilterButton.Click += new System.EventHandler(this.ukloniFilterButton_Click);
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(-2, 399);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(263, 2);
            this.label5.TabIndex = 6;
            // 
            // postaviFilterButton
            // 
            this.postaviFilterButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.postaviFilterButton.Location = new System.Drawing.Point(3, 358);
            this.postaviFilterButton.Name = "postaviFilterButton";
            this.postaviFilterButton.Size = new System.Drawing.Size(257, 38);
            this.postaviFilterButton.TabIndex = 5;
            this.postaviFilterButton.Text = "Postavi Filtar";
            this.postaviFilterButton.UseVisualStyleBackColor = false;
            this.postaviFilterButton.Click += new System.EventHandler(this.postaviFilterButton_Click);
            // 
            // tipComboBox
            // 
            this.tipComboBox.FormattingEnabled = true;
            this.tipComboBox.Location = new System.Drawing.Point(6, 83);
            this.tipComboBox.Name = "tipComboBox";
            this.tipComboBox.Size = new System.Drawing.Size(207, 21);
            this.tipComboBox.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tip:";
            // 
            // brandComboBox
            // 
            this.brandComboBox.FormattingEnabled = true;
            this.brandComboBox.Location = new System.Drawing.Point(6, 39);
            this.brandComboBox.Name = "brandComboBox";
            this.brandComboBox.Size = new System.Drawing.Size(207, 21);
            this.brandComboBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Brand:";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(264, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Filtriranje";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 511);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.dodajStrojButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mainMenu;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "MacRecord";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.filterPanel.ResumeLayout(false);
            this.filterPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem servisiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nadolazeciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nenadaniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obavljeniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistikaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomoćToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button dodajStrojButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutStrojevi;
        private System.Windows.Forms.Panel filterPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ukloniFilterButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button postaviFilterButton;
        private System.Windows.Forms.ComboBox tipComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox brandComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox manjeOdTextBox;
        private System.Windows.Forms.TextBox viseOdTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
    }
}