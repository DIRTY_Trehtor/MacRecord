﻿using MacRecord.Controllers;
using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.AppStarter
{
    public partial class MainForm : Form
    {
        public MainFormController mainFormController;

        public MainForm(MainFormController mainFormController)
        {
            this.mainFormController = mainFormController;

            InitializeComponent();
            initializeMachinesComponent();
            initializeFilterComponent();

            //center
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        private void initializeMachinesComponent()
        {
            //clear
            this.flowLayoutStrojevi.Controls.Clear();
            //fill
            IList<Machine> machines = this.mainFormController.getMachines();
            int i = 0;
            foreach(Machine machine in machines)
            {
                addPanelToMachinesList(machine, i);
                i++;
            }
        }
        private void addPanelToMachinesList(Machine machine, int index)
        {
            int machineId = machine.id;

            Panel panel = new Panel();
            panel.Name = "panel;" + machineId;
            panel.Size = new System.Drawing.Size(flowLayoutStrojevi.Width - 50, 50);
            panel.TabIndex = index;
            panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel.BackColor = Color.WhiteSmoke;

            Label label = new Label();
            label.Text = machine.name + "(" + machine.brand.name + "," + machine.model + ")" + " [" + machine.recentHours + "/" + machine.betweenServiceLimit + "]";
            label.Dock = DockStyle.Left;
            label.Width = 350;
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            label.AutoSize = false;
            label.TextAlign = ContentAlignment.MiddleLeft;

            TextBox newHoursTextBox = new TextBox();
            newHoursTextBox.Dock = DockStyle.Right;
            newHoursTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F);
            newHoursTextBox.Name = "newHoursTextBox;" + index;
            newHoursTextBox.TabIndex = 1;
            //dinamički obradi klik
            newHoursTextBox.KeyPress += (s, e) => {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            };

            Button newHoursButton = new Button();
            newHoursButton.Dock = DockStyle.Right;
            newHoursButton.Name = "newHoursButton;" + index;
            newHoursButton.TabIndex = 2;
            newHoursButton.Text = "<= Odradi sate";
            newHoursButton.UseVisualStyleBackColor = true;
            newHoursButton.Padding = new Padding(0, 0, 5, 0);
            newHoursButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            //dinamički obradi klik
            newHoursButton.Click += (obj, eArgs) =>
            {
                bool result = this.mainFormController.addNewWorkingHours(machine, newHoursTextBox.Text);
                if (result)
                {
                    label.Text = machine.name + "(" + machine.brand.name + "," + machine.model + ")" + " [" + machine.recentHours + "/" + machine.betweenServiceLimit + "]";
                    newHoursTextBox.Text = "";
                }
            };

            Button detailsButton = new Button();
            detailsButton.Dock = DockStyle.Right;
            detailsButton.Name = "detailsButton;" + index;
            detailsButton.TabIndex = 3;
            detailsButton.Text = "Dodatno...";
            detailsButton.UseVisualStyleBackColor = true;
            //dinamički obradi klik
            detailsButton.Click += (obj, eArgs) =>
            {
                this.mainFormController.openAboutMachineView(machine);
                //update promjena(stroj mjenjan)
                initializeMachinesComponent();
            };

            Button deleteButton = new Button();
            deleteButton.Dock = DockStyle.Right;
            deleteButton.Name = "deleteButton;" + index;
            deleteButton.TabIndex = 4;
            deleteButton.Text = "Izbriši";
            deleteButton.UseVisualStyleBackColor = true;
            deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            //dinamički obradi klik
            deleteButton.Click += (obj, eArgs) =>
            {
                this.mainFormController.deleteMachine(machine);
                //update promjena(stroj izbrisan)
                initializeMachinesComponent();
            };

            //hijerarhija
            panel.Controls.Add(label);
            panel.Controls.Add(newHoursTextBox);
            panel.Controls.Add(newHoursButton);
            panel.Controls.Add(detailsButton);
            panel.Controls.Add(deleteButton);
            this.flowLayoutStrojevi.Controls.Add(panel);
        }
        private void initializeFilterComponent()
        {
            Dictionary<int, string> dict1 = new Dictionary<int, string>();
            foreach(var el in this.mainFormController.getMachineTypes())
            {
                dict1.Add(el.Key, el.Value);
            }
            dict1.Add(-1, "- prazno -");

            tipComboBox.DataSource = new BindingSource(dict1, null);
            tipComboBox.DisplayMember = "Value";
            tipComboBox.ValueMember = "Key";

            IList<MachineBrand> brands = this.mainFormController.getMachineBrands();
            if(brands.Count > 0)
            {
                Dictionary<string, MachineBrand> dict2 = new Dictionary<string, MachineBrand>();
                dict2.Clear();
                foreach (MachineBrand brand in brands)
                {
                    dict2.Add(brand.name, brand);
                }
                dict2.Add("- prazno -", null);

                brandComboBox.DataSource = new BindingSource(dict2, null);
                brandComboBox.DisplayMember = "Key";
                brandComboBox.ValueMember = "Value";
            }
            else
            {
                this.brandComboBox.Enabled = false;
            }

            manjeOdTextBox.KeyPress += (s, e) => {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            };
            viseOdTextBox.KeyPress += (s, e) => {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            };
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        private void dodajStrojButton_Click(object sender, EventArgs e)
        {
            this.mainFormController.openAddMachineView();
            //update promjena(novi stroj)
            initializeMachinesComponent();
            //update novih brandova
            initializeFilterComponent();
        }

        private void obavljeniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.mainFormController.openServicesView();
        }

        private void nenadaniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.mainFormController.openProcessServiceView();
            //update promjena(sati)
            initializeMachinesComponent();
        }

        private void nadolazeciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.mainFormController.openPendingServicesView();
            //update promjena(sati)
            initializeMachinesComponent();
        }

        private void postaviFilterButton_Click(object sender, EventArgs e)
        {

            int tip = (int)this.tipComboBox.SelectedValue;
            MachineBrand brand = this.brandComboBox.SelectedValue == null ? null : (MachineBrand)this.brandComboBox.SelectedValue;
            string ukupnoManje = this.manjeOdTextBox.Text;
            int ukupnoManjeInt;
            if ("".Equals(ukupnoManje))
                ukupnoManjeInt = -1;
            else
            {
                ukupnoManjeInt = Int32.Parse(ukupnoManje);
            }
            string ukupnoVise = this.viseOdTextBox.Text;
            int ukupnoViseInt;
            if ("".Equals(ukupnoVise))
                ukupnoViseInt = -1;
            else
            {
                ukupnoViseInt = Int32.Parse(ukupnoVise);
            }


            this.mainFormController.setFilter(tip, brand, ukupnoViseInt, ukupnoManjeInt);
            initializeMachinesComponent();
        }

        private void ukloniFilterButton_Click(object sender, EventArgs e)
        {
            this.viseOdTextBox.Text = "";
            this.manjeOdTextBox.Text = "";
            this.mainFormController.removeFilter();
            initializeMachinesComponent();
        }

        private void statistikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Trenutno nije podržano.");
        }

        private void pomoćToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.mainFormController.openHelpView();
        }
    }
}
