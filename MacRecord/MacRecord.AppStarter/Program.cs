﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MacRecord.Controllers;
using MacRecord.ServicesNRepositories;
using MacRecord.Presentation;

namespace MacRecord.AppStarter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DataBaseService dbService = new DataBaseService();
  
            WindowFormsFactory windowFormsFactory = new WindowFormsFactory();
            MachinesRepository machinesRepository = new MachinesRepository(dbService);
            MainFormController mainFormController = new MainFormController(windowFormsFactory, machinesRepository);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(mainFormController));
        }
    }
}
