﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.BaseLib
{
    public interface IProcessServiceController
    {
        Machine getMachine();
        void ProcessService(IProcessServiceView inView);
    }
}
