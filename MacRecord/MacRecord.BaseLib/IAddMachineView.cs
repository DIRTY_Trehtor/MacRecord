﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MacRecord.Model;

namespace MacRecord.BaseLib
{
    public interface IAddMachineView
    {
        bool showDialog();
        string machineName { get; }
        int machineType { get; }
        MachineBrand machineBrand { get; }
        string machineModel { get; }
        string manufYear { get; }
        string overallWorkingHours { get; }
        string recentHours { get; }
        string betweenServiceLimit { get; }

        string brandName { get;}
        string brandCoutry { get;}


    }
}
