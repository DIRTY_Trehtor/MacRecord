﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.BaseLib
{
    public interface IDataBaseService
    {
        //machine, machine brand, service, 

        int addMachine(Machine machine);
        void updateMachine(Machine machine);
        void deleteMachine(Machine machine);
        IList<Machine> getMachines();

        void addMachineBrand(MachineBrand machineBrand);
        void updateMachineBrand(MachineBrand machineBrand);
        void deleteMachineBrand(MachineBrand machineBrand);
        IList<MachineBrand> getMachineBrands();

        void addService(Service machineBrand);
        void updateaddService(Service machineBrand);
        void deleteaddService(Service machineBrand);
        IList<Service> getServices();
    }
}
