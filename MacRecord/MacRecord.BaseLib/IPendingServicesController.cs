﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.BaseLib
{
    public interface IPendingServicesController
    {
        List<Machine> getPendingServices();
        void processService(Machine machine);
    }
}
