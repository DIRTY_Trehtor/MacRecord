﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MacRecord.BaseLib
{
    public interface IAboutMachineView
    {
        DialogResult showDialog();

        string machineName { get; }
        int machineType { get; }
        MachineBrand machineBrand { get; }
        string machineModel { get; }
        string manufYear { get; }
        string overallWorkingHours { get; }
        string recentHours { get; }
        string betweenServiceLimit { get; }

        string newWorkHours { get;  }

    }
}
