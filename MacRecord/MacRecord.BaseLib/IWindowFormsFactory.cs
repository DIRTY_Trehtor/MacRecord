﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.BaseLib
{
    public interface IWindowFormsFactory
    {
        IAddMachineView createAddMachineView(IMachinesRepository machinesRepository);
        IServicesView createServicesView(IMachinesRepository machinesRepository);
        IProcessServiceView createProcessServiceView(IMachinesRepository machinesRepository, IProcessServiceController processServiceController);
        IAboutMachineView createAboutMachineView(IMachinesRepository machinesRepository, IAboutMachineController aboutMatchineController);
        IPendingServicesView createPendingServicesView(IPendingServicesController pendingServicesController);
        IHelpView createHelpView(IHelpController helpController);
    }
}
