﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.BaseLib
{
    public interface IMachinesRepository
    {
        void addMachine(Machine machine);
        void updateMachine(Machine machine, int atIndex);
        void deleteMachine(Machine machine);

        void addMachineBrand(MachineBrand machineBrand);

        void addService(Service service);

        void tryMachineAsPending(Machine machine);
        
        IList<Machine> getMachines();
        IList<Service> getServices();
        List<Machine> getPendingServices();
        IList<MachineBrand> getMachineBrands();
        Dictionary<int, string> getMachineTypes();

        
    }
}
