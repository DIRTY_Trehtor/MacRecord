﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.BaseLib
{
    public interface IAboutMachineController
    {
        Machine getMachine();
        void updateMachine();
        void deleteMachine();
        bool addWorkingHours();
    }
}
