﻿using MacRecord.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.BaseLib
{
    public interface IProcessServiceView
    {
        bool showDialog();
        string opis { get; }
        string datum { get; }
        string serviser { get; }
        Machine machine { get; }

    }
}
