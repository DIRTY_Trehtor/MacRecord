﻿using MacRecord.BaseLib;
using MacRecord.Model;
using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.ServicesNRepositories
{
    public class DataBaseService : IDataBaseService
    {
        private Configuration cfg;
        private SQLiteConnection connection;
        private const string CONNECTION_STRING =
                @"Data Source=nhlite.db;Pooling=true;FailIfMissing=false;
                                BinaryGUID=false;New=false;Compress=true;Version=3";
        private ISessionFactory sessionFactory;
        //private ISession session;

        public DataBaseService()
        {
            Init();
            BuildSchema();
        }
        public void Init()
        {
            // Initialize NHibernate
            cfg = new Configuration();
            string path = Directory.GetCurrentDirectory();
            string newPath = Path.GetFullPath(Path.Combine(path, @"..\..\..\MacRecord.ServicesNRepositories"));
            cfg.Configure(Path.Combine(newPath, "hibernate.cfg.xml"));
            IDictionary<string, string> props = new Dictionary<string, string>();
            props.Add("connection.connection_string", CONNECTION_STRING);
            props.Add("connection.driver_class", "NHibernate.Driver.SQLite20Driver");
            props.Add("dialect", "NHibernate.Dialect.SQLiteDialect");
            props.Add("query.substitutions", "true=1;false=0");
            props.Add("show_sql", "false");
            cfg.SetProperties(props);

            //cfg.AddAssembly(typeof(MacRecord.ServicesNRepositories.MachinesRepository).Assembly);
            cfg.AddAssembly(typeof(MacRecord.Model.Machine).Assembly);

            connection = new SQLiteConnection(CONNECTION_STRING);
            connection.Open();
            
            sessionFactory = cfg.BuildSessionFactory();

            //this.session = this.sessionFactory.OpenSession();
        }

        private void BuildSchema()
        {
            NHibernate.Tool.hbm2ddl.SchemaUpdate schemaUpdate
                = new NHibernate.Tool.hbm2ddl.SchemaUpdate(cfg);

            schemaUpdate.Execute(false, true);
        }

        /* INTERFACE */
        public int addMachine(Machine machine)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Save(machine);
                transaction.Commit();

                return machine.id;
            }
        }

        public void updateMachine(Machine machine)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Update(machine);
                transaction.Commit();
            }
        }

        public void deleteMachine(Machine machine)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Delete(machine);
                transaction.Commit();
            }
        }

        public IList<Machine> getMachines()
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                ICriteria criteria = session.CreateCriteria<Machine>();
                IList<Machine> machines = criteria.List<Machine>();

                transaction.Commit();

                return machines;
            }
        }

        public void addMachineBrand(MachineBrand machineBrand)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Save(machineBrand);
                transaction.Commit();
            }
        }

        public void updateMachineBrand(MachineBrand machineBrand)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Update(machineBrand);
                transaction.Commit();
            }
        }

        public void deleteMachineBrand(MachineBrand machineBrand)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Delete(machineBrand);
                transaction.Commit();
            }
        }

        public IList<MachineBrand> getMachineBrands()
        {
           using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                ICriteria criteria = session.CreateCriteria<MachineBrand>();
                IList<MachineBrand> machineBrands = criteria.List<MachineBrand>();

                transaction.Commit();

                return machineBrands;
            }
        }

        public void addService(Service service)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Save(service);
                transaction.Commit();
            }
        }

        public void updateaddService(Service service)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Update(service);
                transaction.Commit();
            }
        }

        public void deleteaddService(Service service)
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Delete(service);
                transaction.Commit();
            }
        }

        public IList<Service> getServices()
        {
            using (var session = this.sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                ICriteria criteria = session.CreateCriteria<Service>();
                IList<Service> services = criteria.List<Service>();

                transaction.Commit();

                return services;
            }
        }
    }
}
