﻿using MacRecord.BaseLib;
using MacRecord.Model;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacRecord.ServicesNRepositories
{
    public enum MachineType { PoljoprivredniStroj, Alat };

    public class MachinesRepository : IMachinesRepository
    {
        private IList<Machine> machines;
        private IList<Service> services;
        private List<Machine> pendingForService;
        private IList<MachineBrand> brands;
        Dictionary<int, string> machineTypeDict;

        IDataBaseService dbService;

        public MachinesRepository(IDataBaseService dbService)
        {
            this.machines = new List<Machine>();
            this.services = new List<Service>();
            this.pendingForService = new List<Machine>();
            this.brands = new List<MachineBrand>();
            this.machineTypeDict = new Dictionary<int, string>();
            machineTypeDict.Add(1, "Građevinski");
            machineTypeDict.Add(2, "Poljoprivredni");
            machineTypeDict.Add(3, "Alat");

            this.dbService = dbService;

            loadDataFromDB();
        }
        /* UČITAVANJE IZ BAZE */
        public void loadDataFromDB()
        {
            getMachinesFromDB();
            getMachineBrandsFromDB();
            getServicesFromDB();

            checkMachinesForPendingServices();
        }
        private void getMachinesFromDB()
        {
            this.machines = this.dbService.getMachines();
        }
        private void getMachineBrandsFromDB()
        {
            this.brands = this.dbService.getMachineBrands();
        }
        private void getServicesFromDB()
        {
            this.services = this.dbService.getServices();
        }
        private void checkMachinesForPendingServices()
        {
            foreach(Machine machine in machines)
            {
                int limit = machine.betweenServiceLimit;
                int recent = machine.recentHours;
                if (recent > limit * 0.8)
                {
                    this.pendingForService.Add(machine);
                }
            }
        }

        /* INTERFACE */

        public void addMachine(Machine machine)
        {
            //dodaj u repozitorij
            this.machines.Add(machine);
            //zapamti si index
            int index = this.machines.IndexOf(machine);
            //dodaj u bazu
            this.dbService.addMachine(machine);
            //update id-jeve
            this.machines[index].id = machine.id;
            this.machines[index].brand.id = machine.brand.id;

            //check if for pending
            this.tryMachineAsPending(machine);
        }
        public void updateMachine(Machine machine, int atIndex)
        {
            //update u repozitoriju
            this.machines[atIndex] = machine;
            //update u bazi
            this.dbService.updateMachine(machine);
        }

        public void deleteMachine(Machine machine)
        {
            //delete u bazi
            this.dbService.deleteMachine(machine);
            //delete u repozitoriju
            deleteFromMachinesList(machine);
            deleteFromPendingServicesList(machine);
            machine = null;
            
        }
        private void deleteFromMachinesList(Machine machine)
        {
            this.machines.Remove(machine);
        }
        private void deleteFromPendingServicesList(Machine machine)
        {
            this.pendingForService.Remove(machine);
        }
        public void addService(Service service)
        {
            //remove pending
            Machine servicedMachine = service.servicedMachine;
            deleteFromPendingServicesList(servicedMachine);
            //resset hours
            int index = this.machines.IndexOf(servicedMachine);
            this.machines[index].recentHours = 0;
            //add processed
            this.services.Add(service);

            //DB: add service, update machine hours
            this.dbService.addService(service);
            this.dbService.updateMachine(servicedMachine);
        }
        public void addMachineBrand(MachineBrand machineBrand)
        {
            //add to DB
            this.dbService.addMachineBrand(machineBrand);
            //add to repository
            this.brands.Add(machineBrand);
        }
        public IList<Machine> getMachines()
        {
            return this.machines;
        }
        public IList<Service> getServices()
        {
            return this.services;
        }
        public List<Machine> getPendingServices()
        {
            return this.pendingForService;
        }
        public IList<MachineBrand> getMachineBrands()
        {
            return this.brands;
        }
        
        public Dictionary<int, string> getMachineTypes()
        {
            return this.machineTypeDict;
        }

        public void tryMachineAsPending(Machine machine)
        {
            int recent = machine.recentHours;
            int limit = machine.betweenServiceLimit;

            bool contains = this.pendingForService.Contains(machine);
            if(!contains)
                if (recent > limit * 0.8)
                    this.pendingForService.Add(machine);
        }
    }
    
}
